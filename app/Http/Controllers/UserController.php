<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Models\User;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{

    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware('ajax')->only('destroy');
    }

    public function index()
    {
        $users = $this->repository->getAllWithPhotosCount();
        return view ('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit(User $user)
    {
        return view ('users.edit', compact ('user'));
    }

    public function update(UserRequest $request, User $user)
    {
        $this->repository->update ($user, $request);
        return redirect ()->route('user.index')->with ('ok', __ ("L'utilisateur a bien été modifié"));
    }

    public function destroy(User $user)
    {
        $user->delete ();
        return response ()->json ();
    }
}
